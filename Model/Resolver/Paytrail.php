<?php
/**
 * ScandiWeb_PaytrailGraphQl
 *
 * @category ScandiWeb
 * @package  ScandiWeb\PaytrailGraphQl
 * @author   Akim Goncar <info@scandiweb.com>
 */
declare(strict_types=1);

namespace Scandiweb\PaytrailGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Payment\Model\Config;

/**
 * Class Paytrail
 *
 * @package ScandiWeb\OmnivaltGraphQl\Model\Resolver
 */
class Paytrail implements ResolverInterface
{
    const PAYMENT_METHOD_CODE = 'paytrail';

    /**
     * @var
     */
    protected $paymentConfig;

    /**
     * Fetches the data from persistence models and format it according to the GraphQL schema.
     *
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     *
     * @return mixed|Value
     * @throws \Exception
     */

    public function __construct(
        Config $paymentConfig
    )
    {
        $this->paymentConfig = $paymentConfig;
    }

    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    )
    {

        $data = [];

        if ($value['code'] == self::PAYMENT_METHOD_CODE) {

            $payment = $this->paymentConfig->getActiveMethods();
            $paytrail = $payment[self::PAYMENT_METHOD_CODE];
            $paytrailGroups = $paytrail->getEnabledPaymentMethodGroups();

            foreach ($paytrailGroups as $group) {
                $group_data = [
                    'id' => $group['id'],
                    "title" => $group['title']->getText(),
                    'paytrail_methods' => []
                ];

                $group_methods = $group['methods'];

                foreach ($group_methods as $method) {
                    $method_data = [
                        'title' => $method['title']->getText(),
                        'id' => $method['id']
                    ];
                    array_push($group_data['paytrail_methods'], $method_data);
                }
                array_push($data, $group_data);
            }
        }
        return $data;
    }
}
