import axios from 'axios';
import { DETAILS_STEP } from 'Route/Checkout/Checkout.component';
import { fetchQuery, fetchMutation } from 'Util/Request';
import CheckoutQuery from 'Query/Checkout.query';
import { isSignedIn } from 'Util/Auth';
import { PAYTRAIL_CODE } from '../config/config';
import BrowserDatabase from 'Util/BrowserDatabase';

export const PAYMENT_TOTALS = 'PAYMENT_TOTALS';

class CheckoutContainerPlugin extends ExtensibleClass {
    containerFunctions = (originalMember, instance) => {
        return {
            ...originalMember,
            redirectSuccess: this.redirectSuccess.bind(instance)
        }
    }

    redirectSuccess(orderID) {
        this.setState({
            orderID,
            checkoutStep: DETAILS_STEP,
            isLoading: false
        });
    }

    savePaymentMethodAndPlaceOrder = async (args, callback, instance) => {
        const { paytrail_method_code } = instance.props;
        const [ paymentInformation ] = args;
        const { paymentMethod: { code, additional_data } } = paymentInformation;

        if (code !== PAYTRAIL_CODE) {
            callback.apply(instance, args);
            return;
        }
        const guest_cart_id = !isSignedIn() ? instance._getGuestCartId() : '';

        try {
            await fetchMutation(CheckoutQuery.getSetPaymentMethodOnCartMutation({
                guest_cart_id,
                payment_method: {
                    code, [code]: additional_data
                }
            }));

            const orderData = await fetchMutation(CheckoutQuery.getPlaceOrderMutation(guest_cart_id));
            const { placeOrder: { order: { order_id } } } = orderData;

            instance.setDetailsStep(order_id, code);
        } catch (e) {
            instance._handleError(e);
        }
    }

    setDetailsStep = (args, callback, instance) => {
        const { paytrail_method_code, resetCart } = instance.props;
        const [orderID, code] = args;

        if (code !== PAYTRAIL_CODE) {
            callback.apply(instance, args);
            return;
        }
        BrowserDatabase.deleteItem(PAYMENT_TOTALS);
        resetCart();
        instance.setState({
            isLoading: true,
            paymentTotals: {},
            orderID
        });

        this.getPaytrail(instance, paytrail_method_code);
    }

    getPaytrail(context, id) {
        const { orderID } = context.state;

        const config = {
            headers: {
                'Pragma': 'no-cache',
                'Cache-Control': 'no-cache' },
            params: {
                is_ajax: true,
                preselected_payment_method_id: id,
                orderId: orderID
            }
        }

        axios.get('/paytrail/redirect', config)
            .then(res => {
                this.redirect(res.data);
            })
            .catch(console.error);
    }

    redirect(response){
        const { data, success } = response;

        if (success) {
            const div = document.createElement('div');

            div.innerHTML = data;
            document.body.appendChild(div);
            document.getElementById('paytrail_form').submit();
        }
    }
}

class CheckoutStatePlugin {
    mapStateToProps = (args, callback, instance) => {
        const [state] = args;

        return {
            ...callback.apply(instance, args),
            paytrail_method_code: state.PaytrailReducer.paytrail
        }
    }
}

const {
    savePaymentMethodAndPlaceOrder,
    containerFunctions,
    setDetailsStep
} = new CheckoutContainerPlugin();

const { mapStateToProps } = new CheckoutStatePlugin();


const config = {
    'Route/Checkout/Container/mapStateToProps': {
        'function': [
            {
                position: 93,
                implementation: mapStateToProps
            }
        ],
    },
    'Route/Checkout/Container': {
        'member-function': {
            savePaymentMethodAndPlaceOrder: [
                {
                    position: 100,
                    implementation: savePaymentMethodAndPlaceOrder
                }
            ],
            setDetailsStep: [
                {
                    position: 101,
                    implementation: setDetailsStep
                }
            ]
        },
        'member-property': {
            'containerFunctions': [
                {
                    position: 110,
                    implementation: containerFunctions
                }
            ]
        },
    }
};

export default config;
