class CheckoutComponentPlugin extends ExtensibleClass {
    componentDidMount = (args, callback, instance) => {
        this.redirectSuccess(instance);
        callback.apply(instance, args);
    }

    state = (originalMember, instance) => {
        return {
            ...originalMember,
            orderId: undefined
        }
    }

    redirectSuccess(context) {
        const { redirectSuccess } = context.props;
        const getOrderId = (new URLSearchParams(window.location.search)).get('orderId');

        if (!getOrderId) {
            return
        } else {
            const idOrder = getOrderId.replace('/', '');
            redirectSuccess(idOrder);
        }
    }
}

const { componentDidMount, state } = new CheckoutComponentPlugin();

const config = {
    'Route/Checkout/Component': {
        'member-property': {
            'state': [
                {
                    position: 111,
                    implementation: state
                }
            ]
        },
        'member-function': {
            componentDidMount: [
                {
                    position: 112,
                    implementation: componentDidMount
                }
            ]
        }
    }
}

export default config;
