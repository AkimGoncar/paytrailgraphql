import PayTrail from '../component/Paytrail';

export const PAYTRAIL = 'paytrail';

class CheckoutPaymentsPlugin {
    renderPaytrail() {
        const { paymentMethods } = this.props;
        const paytrail_method = paymentMethods.find(method => method.code == PAYTRAIL);

        return (
            <PayTrail
                paytrail = { paytrail_method.paytrail }
            />
        );
    }

    paymentRenderMap = (originalMember, instance) => {
        return {
            ...originalMember,
            [PAYTRAIL]: this.renderPaytrail.bind(instance)
        }
    }
}

const {
    paymentRenderMap,
} = new CheckoutPaymentsPlugin();

const config = {
    'Component/CheckoutPayments/Component': {
        'member-function': {
            'paymentRenderMap': [
                {
                    position: 100,
                    implementation: paymentRenderMap
                }
            ]
        }
    }
};

export default config;
