import { Field } from 'Util/Query';

class CheckoutQueryPlugin extends ExtensibleClass {
    _getPaymentMethodFields = (args, callback, instance) => {

        return [
            ...callback.apply(instance, args),
            this.getPaytrail()
        ];
    }

    getPaytrail(){
        return new Field('paytrail')
            .addFieldList(this.getPaytrailGroups())
    }

    getPaytrailGroups() {
        return [
            'id',
            'title',
            this.getPaytrailMethods()
        ]
    }

    getPaytrailMethods(){
        return new Field('paytrail_methods')
            .addFieldList(this.getPaytrailMethodsFields())
    }

    getPaytrailMethodsFields() {
        return [
            'title',
            'id'
        ]
    }
}

const { _getPaymentMethodFields } = new CheckoutQueryPlugin();

const config = {
    'Query/Checkout': {
        'member-function': {
            _getPaymentMethodFields: [
                {
                    position: 123,
                    implementation: _getPaymentMethodFields
                }
            ]
        }
    }
};

export default config;
