import { PaytrailReducer } from '../store/Paytrail';

class PaytrailReducerPlugin extends ExtensibleClass {
    getReducer = (args, callback, instance) => ({
        ...callback.apply(instance, args),
        PaytrailReducer
    })
}

const { getReducer } = new PaytrailReducerPlugin();

const config = {
    'Store/Index/getReducers': {
        'function': [
            {
                position: 92,
                implementation: getReducer
            }
        ]
    },
}

export default config;
