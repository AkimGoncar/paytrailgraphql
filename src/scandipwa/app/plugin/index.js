/*
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandiweb/paytrail-scandipwa
 * @author    Akims Goncars <info@scandiweb.com>
 */


import CheckoutComponentPlugin from './Checkout.component.plugin';
import CheckoutContainerPlugin from './Checkout.container.plugin';
import CheckoutStatePlugin from './Checkout.container.plugin';
import CheckoutPaymentsPlugin from './CheckoutPayments.plugin';
import CheckoutQueryPlugin from './CheckoutQuery.plugin';
import PaytrailReducerPlugin from './PaytrailStoreReducer.plugin';
import UrlHandlerPlugin from './UrlHandler.plugin';

export default {
    ...CheckoutComponentPlugin,
    ...CheckoutContainerPlugin,
    ...CheckoutStatePlugin,
    ...CheckoutPaymentsPlugin,
    ...CheckoutQueryPlugin,
    ...PaytrailReducerPlugin,
    ...UrlHandlerPlugin
};
