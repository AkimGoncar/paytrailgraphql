class UrlHandlerPlugin extends ExtensibleClass {
    /**
     * Allow send data to this hosts without cache
     * Should be in regex format. Example: (?!^.*cloudpayments\.ru)
     *
     * @type {Array}
     */
    getBypassCacheHosts = (args, callback, instance) => ([
        '(?!^.*paytrail)', // Paytrail Payment Method
        ...callback.apply(instance, args),
    ])
}

const { getBypassCacheHosts } = new UrlHandlerPlugin();

const config = {
    'SW/Handler/UrlHandler/getBypassCacheHosts': {
        'function': [
            {
                position: 93,
                implementation: getBypassCacheHosts
            }
        ]
    },
}

export default config;
