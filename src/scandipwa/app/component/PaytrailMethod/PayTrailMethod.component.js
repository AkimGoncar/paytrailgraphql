/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

import PropTypes from 'prop-types';
import images from './ImageConfig';

import './PayTrailMethod.style';

export class PayTrailMethod extends ExtensiblePureComponent {
    static propTypes = {
        PayTrailMethod: PropTypes.object.isRequired,
        onClick: PropTypes.func.isRequired,
        isSelected: PropTypes.bool

    };

    onClick = (id) => {
        const {
            onClick
        } = this.props;

        onClick(id);
    };

    getImage(methodId, alt) {
        const method_image = images.find(images => images.id == methodId);
        const { src } = method_image;

        return (
            <img src={require(`${src}`)} alt={ alt } />
        );
    }

    render() {
        const { PayTrailMethod: { title, id }, isSelected } = this.props;
        return (
            <div block="PayTrailMethod">
                <div block="PayTrailMethod" elem="Title">
                    <button
                        block="PayTrailMethod"
                        mods={{ isSelected }}
                        elem="Button"
                        value={ id }
                        onClick={() => this.onClick(id)}
                        type="button"
                    >
                        { this.getImage(id, title) }
                    </button>
                </div>
            </div>
        );
    }
}

export default middleware(PayTrailMethod, 'Component/PayTrailMethod/Component');
