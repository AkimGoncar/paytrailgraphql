import PropTypes from 'prop-types';
import PayTrailMethod from './PayTrailMethod.component';

export class PayTrailMethodContainer extends ExtensiblePureComponent {
    static propTypes = {
        PayTrailMethod: PropTypes.object,
        isSelected: PropTypes.bool,
        onClick: PropTypes.func.isRequired
    };

    static defaultProps = {
        PayTrailMethod: {},
        isSelected: false
    };

    render() {
        return (
            <PayTrailMethod
              { ...this.props }
            />
        );
    }
}

export  default middleware(PayTrailMethodContainer, 'Component/PayTrailMethod/Container');
