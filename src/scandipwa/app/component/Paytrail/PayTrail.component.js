/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

import PropTypes from 'prop-types';

import PayTrailMethod from '../PaytrailMethod'

import './PayTrail.style';
export class PayTrail extends ExtensiblePureComponent {
    static propTypes = {
        paytrail: PropTypes.array.isRequired,
        selectPaymentMethod: PropTypes.func.isRequired,
        selectedPaymentCode: PropTypes.number.isRequired
    };

    state = {
        selectedGroup: '',
    }

    onClick = (value) => {
        this.setState({
            selectedGroup: value
        });
    }

    renderPaytrailMethod(paytrail_method, index) {
        const {
            selectPaymentMethod,
            selectedPaymentCode
        } = this.props;

        const { id } = paytrail_method;

        const isSelected = selectedPaymentCode == id;

        return (
            <div block="PayTrail" elem="MethodWrapper" key={index}>
                <PayTrailMethod
                    PayTrailMethod={paytrail_method}
                    isSelected={ isSelected }
                    onClick={ selectPaymentMethod }
                />
            </div>
        );
    }

    renderGroupMethods(group) {
        const { paytrail_methods } = group;

        return (
            paytrail_methods.map((paytrail_method, index) =>
                this.renderPaytrailMethod(paytrail_method, index)
            )
        );
    }

    renderGroup(group) {
        const { selectedGroup } = this.state;
        const { id, title } = group;

        const isSelected = selectedGroup === id;

        return (
            <div block="PayTrail" elem="Group" key={id}>
                <button
                    block="PayTrail"
                    mods={{ isSelected }}
                    elem="Button"
                    value={id}
                    onClick={() => this.onClick(id)}
                    type="button"
                >
                    {title}
                </button>
                <div block="PayTrail" elem="Methods" key={ id }>
                    {
                        isSelected ? (
                            this.renderGroupMethods(group)
                        ) : (null)
                    }
                </div>
            </div>
        );
    }

    render() {
        const { paytrail } = this.props;

        return (
            <div block="PayTrail">
                {paytrail.map(group => {
                    return this.renderGroup(group);
                }) }
            </div>
        );
    }
}

export default middleware(PayTrail, 'Component/PayTrail/Component');
