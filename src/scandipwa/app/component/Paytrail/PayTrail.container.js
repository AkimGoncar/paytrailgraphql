import PropTypes from 'prop-types';
import PayTrail from './PayTrail.component';
import { connect } from 'react-redux';
import { PaytrailDispatcher } from '../../store/Paytrail';

export const mapDispatchToProps = dispatch => ({
    updatePaytrail: (options) => {
        PaytrailDispatcher.updatePaytrail(dispatch, options);
    }
});

export class PayTrailContainer extends ExtensiblePureComponent {
    static propTypes = {
        updatePaytrail: PropTypes.func.isRequired,
        paytrail: PropTypes.array
    };

    static defaultProps = {
        paytrail: [],
        selectedPaymentCode: 0
    };

    containerFunctions = {
        selectPaymentMethod: this.selectPaymentMethod.bind(this)
    }

    selectPaymentMethod(id) {
        const { updatePaytrail } = this.props;
        this.setState({
            selectedPaymentCode: id
        });

        updatePaytrail({ paytrail: id });
    }

    render() {
        return (
            <PayTrail
              { ...this.props }
              { ...this.containerFunctions }
              { ...this.state }
            />
        );
    }
}

export default connect(null, mapDispatchToProps)(middleware(PayTrailContainer, 'Component/PayTrail/Container'));
