/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

import {
    UPDATE_PAYTRAIL
} from './Paytrail.action';

export const initialState = {
    paytrail: 0
};

export const PaytrailReducer = (state = initialState, action) => {
    switch (action.type) {
    case UPDATE_PAYTRAIL:
        const { paytrail } = action;

        return { paytrail };

    default:
        return state;
    }
};

export default PaytrailReducer;
