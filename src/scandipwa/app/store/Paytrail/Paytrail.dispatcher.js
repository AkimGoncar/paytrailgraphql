/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

import { updatePaytrail } from './';
/**
 * Paytrail Dispatcher
 * @class PaytrailDispatcher
 */
export class PaytrailDispatcher extends ExtensibleClass {
    updatePaytrail(dispatch, options) {
        const { paytrail } = options;
        dispatch(updatePaytrail(paytrail));
    }
}

export default new (middleware(PaytrailDispatcher, 'Store/Paytrail/Dispatcher'))();
