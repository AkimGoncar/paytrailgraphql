/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

export const UPDATE_PAYTRAIL = 'UPDATE_PAYTRAIL';

/**
 * Update router to show Paytrail page
 * @param  {Boolean} Paytrail New Paytrail value
 * @return {void}
 */
export const updatePaytrail = paytrail => ({
    type: UPDATE_PAYTRAIL,
    paytrail
});
