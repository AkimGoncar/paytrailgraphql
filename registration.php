<?php
/**
 * ScandiWeb_PaytrailGraphQl
 *
 * @category ScandiWeb
 * @package  ScandiWeb\PaytrailGraphQl
 * @author   Akim Goncar <info@scandiweb.com>
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'ScandiWeb_PaytrailGraphQl',
    __DIR__
);
